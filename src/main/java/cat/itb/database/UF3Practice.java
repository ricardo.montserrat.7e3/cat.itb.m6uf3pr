package cat.itb.database;

import cat.itb.pojos.Restaurant;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import java.util.Arrays;

public class UF3Practice
{
    public static final String DATABASE_NAME = "itb";
    public static final String COLLECTION_NAME = "restaurants";

    public static final String SERVER_MONGO_IP = "mongodb+srv://user1:9878JCv@cluster0.yjrbj.azure.mongodb.net/" + DATABASE_NAME +"?retryWrites=true&w=majority";
    public static final String VIRTUAL_MACHINE_IP = "192.168.56.101:27017";

    private final MyMongoDatabase<Restaurant> mongo = new MyMongoDatabase<>(VIRTUAL_MACHINE_IP, Restaurant.class, false);

    public void deleteDatabase(final String database, final String collection)
    {
        System.out.println("Deleting database " + database + "...");
        System.out.println("Dropped collection of size: " + mongo.dropCollection(database, collection));
        System.out.println("\n\nCollections left are ->\n{");
        mongo.showCollections(database);
        System.out.println("}");
    }

    public void createDatabase(final String database, final String collection, final String filePath)
    {
        System.out.println("Creating database " + database + "...");
        System.out.println("Inserted: " + mongo.loadArrayOfDatabaseType(database, collection, filePath) + " documents.");
    }

    public void insertRestaurants(final Restaurant... restaurants)
    {
        System.out.println("\nInserting into database " + DATABASE_NAME + " and collection " + COLLECTION_NAME + ": the restaurants -> {");
        for (Restaurant restaurant : restaurants) System.out.println(restaurant + ", ");
        System.out.println("}");
        if (restaurants.length == 1) mongo.insertDocumentOfDatabaseType(DATABASE_NAME, COLLECTION_NAME, restaurants[0]);
        else mongo.insertDocumentsOfDatabaseType(DATABASE_NAME, COLLECTION_NAME, restaurants);
    }

    public void showSeafoodRestaurantsOnManhattan()
    {
        System.out.println("Looking for Seafood restaurants that are on Manhattan... -> \n{");
        mongo.showDocumentsWhere("borough", "Manhattan", DATABASE_NAME, COLLECTION_NAME, 0, Filters.eq("cuisine", "Seafood"));
        System.out.println("}");
    }

    public void showRestaurantsNameWithZipcode(final String zipcode)
    {
        System.out.println("Looking for restaurants with zipcode " + zipcode + "... -> \n{");
        mongo.showDocumentsWhere("address.zipcode", zipcode, DATABASE_NAME, COLLECTION_NAME, 0, "name");
        System.out.println("}");
    }

    public void showRestaurantsNameWithZipcodeAndStreet(final String zipcode, final String street)
    {
        System.out.println("Looking for restaurants with zipcode " + zipcode + " and with street " + street + "... -> \n{");
        mongo.showDocumentsWhere("address.zipcode", zipcode, DATABASE_NAME, COLLECTION_NAME, 0, Filters.eq("address.street", street), "name");
        System.out.println("}");
    }

    public void updateZipcodeByStreet(final String street, final String zipcode)
    {
        System.out.println("Updating restaurants zipcode with street name as " + street + "...");
        System.out.print("Updated a total of: " + mongo.updateDocuments(DATABASE_NAME, COLLECTION_NAME, Filters.eq("address.street", street), Updates.set("address.zipcode", zipcode)));
        System.out.println(" restaurants.");
    }

    public void insertFieldToRestaurantsOfCuisine(final String fieldName, final String fieldValue, final String cuisine)
    {
        System.out.println("Inserting field " + fieldName + " with value " + fieldValue + " to restaurants where cuisine equals " + cuisine + "...");
        System.out.print("Updated a total of: " + mongo.setField(fieldName, fieldValue, DATABASE_NAME, COLLECTION_NAME, Filters.eq("cuisine", cuisine)));
        System.out.println(" restaurants. -> \n{");
        mongo.showDocumentsWhere("cuisine", cuisine, DATABASE_NAME, COLLECTION_NAME, 0, "name", "cuisine", "stars", "restaurantId");
        System.out.println("}");
    }

    public void deleteAllRestaurantsOfCuisine(final String cuisine)
    {
        System.out.println("Deleting restaurants of cuisine " + cuisine + "...");
        System.out.print("Deleted a total of: " + mongo.deleteDocuments(DATABASE_NAME, COLLECTION_NAME, Filters.eq("cuisine", cuisine)));
        System.out.println(" restaurants.");
    }

    public void showRestaurantsQuantityByBorough()
    {
        System.out.println("Organizing restaurants by group");
        mongo.showDocumentsByGroupSize("borough", DATABASE_NAME, COLLECTION_NAME);
    }

    public void showBoroughWithLessRestaurants()
    {
        mongo.getCollection(DATABASE_NAME, COLLECTION_NAME).aggregate(Arrays.asList(Aggregates.group("$borough", Accumulators.min("lessRestaurants", "$borough")), Aggregates.limit(1)))
                .iterator().forEachRemaining(System.out::println);
    }
}
