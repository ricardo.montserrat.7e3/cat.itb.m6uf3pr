package cat.itb;

import cat.itb.database.UF3Practice;
import cat.itb.pojos.Address;
import cat.itb.pojos.Restaurant;
import cat.itb.utilities.Tools;
import cat.itb.utilities.Tools.Menu;
import cat.itb.utilities.Tools.Menu.MenuOption;

import java.util.ArrayList;
import java.util.Arrays;

public class Main
{
    private static final String JSONS_PATH = "src/main/resources/JSONS/";

    public void start()
    {
        UF3Practice practice = new UF3Practice();

        //Creation of menu actions
        Menu.MenuAction deleteDatabase = () -> practice.deleteDatabase(UF3Practice.DATABASE_NAME, UF3Practice.COLLECTION_NAME);
        Menu.MenuAction createDatabase = () -> practice.createDatabase(UF3Practice.DATABASE_NAME, UF3Practice.COLLECTION_NAME, JSONS_PATH + "restaurants.json");
        Menu.MenuAction insertRestaurants = () -> practice.insertRestaurants(
                new Restaurant(new Address("10312", "Richmond Avenue", "3850"), "999999999", "David Vaquer - El pizzero espagnol", "Pizza", "Staten Island", new ArrayList<>()),
                new Restaurant(new Address("10016", Arrays.asList(-73.9812198, 40.7509706), "East 39 Street", "24"), "999999991", "Joan_Gomez's Hamburgers", "American", "Manhattan", new ArrayList<>())
        );
        Menu.MenuAction showWithZipcode = () -> practice.showRestaurantsNameWithZipcode(Tools.inputAny("\nPlease write the zipcode to look for: "));
        Menu.MenuAction showWithZipcodeAndStreet = () -> practice.showRestaurantsNameWithZipcodeAndStreet(
                Tools.inputAny("\nPlease, write the zipcode to look for: "),
                Tools.inputAny("\nPlease, write the street to look for: ")
        );
        Menu.MenuAction updateZipcodeCharlesStreet = () -> practice.updateZipcodeByStreet("Charles Street", "30033");
        Menu.MenuAction fieldStarsByCuisine = () -> practice.insertFieldToRestaurantsOfCuisine("stars", "*****", "Caribbean");
        Menu.MenuAction deleteByCuisines = () -> practice.deleteAllRestaurantsOfCuisine("Delicatessen");

        createDatabase.doAction();

        //Menu creation
        Menu menu = new Menu("M6UF3 Practice", "Please, select an option: ", "That's not an existing option!", "\n\nGoodbye! :D", true,
                new MenuOption("Create collection restaurants for itb.", createDatabase, "exc1"),
                new MenuOption("Delete collection restaurants from itb.", deleteDatabase, "exc2"),
                new MenuOption("Insert on collection restaurants 2 restaurants.", insertRestaurants, "exc3"),
                new MenuOption("Show seafood restaurants on Manhattan.", practice::showSeafoodRestaurantsOnManhattan, "exc4"),
                new MenuOption("Show restaurants with zipcode passed.", showWithZipcode, "exc5"),
                new MenuOption("Show restaurants with zipcode and street passed.", showWithZipcodeAndStreet, "exc6"),
                new MenuOption("Update restaurants zipcode to 30033 in the street Charles Street.", updateZipcodeCharlesStreet, "exc7"),
                new MenuOption("Set 5 stars field to all restaurants of cuisine Caribbean.", fieldStarsByCuisine, "exc8"),
                new MenuOption("Delete all restaurants of cuisine Delicatessen.", deleteByCuisines, "exc9"),
                new MenuOption("Show boroughs, group by restaurants quantity.", practice::showRestaurantsQuantityByBorough, "ex10"),
                new MenuOption("Show borough with less restaurants..", practice::showBoroughWithLessRestaurants, "ex10")
        );

        menu.setOnDestroyAction(deleteDatabase);

        menu.startMenu();
    }

    public static void main(String[] args)
    {
        Main program = new Main();
        program.start();
    }
}
