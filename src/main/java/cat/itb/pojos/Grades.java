package cat.itb.pojos;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class Grades
{
    @SerializedName("date")
    private Date date;

    @SerializedName("score")
    private int score;

    @SerializedName("grade")
    private String grade;
}