package cat.itb.pojos;

import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@ToString
public class Address
{
    @SerializedName("zipcode")
    @NonNull
    private String zipcode;

    @SerializedName("coord")
    private List<Double> coord;

    @SerializedName("street")
    @NonNull
    private String street;

    @SerializedName("building")
    @NonNull
    private String building;
}