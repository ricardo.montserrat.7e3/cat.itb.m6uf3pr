package cat.itb.pojos;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Restaurant
{
    @SerializedName("address")
    private Address address;

    @SerializedName("restaurant_id")
    private String restaurantId;

    @SerializedName("name")
    private String name;

    @SerializedName("cuisine")
    private String cuisine;

    @SerializedName("borough")
    private String borough;

    @SerializedName("grades")
    private List<Grades> grades;
}